//let/var/const and function key words are not allowed inside a class

var a = 10; //global variable
class A {
    b = 12;//class variable
    fun(): void {
        var c = 20;//local variable
        console.log('local variable in function ' + c);
    }
}

console.log('Global variable a='+a);
var objA=new A();
console.log('class variable b'+objA.b);
objA.fun();