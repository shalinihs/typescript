class Employee{
    constructor(){
        console.log('We are in Employee Constructor');
    }
   
}
class Emp{
    constructor(id:any){
        console.log('We are in Emp Constructor Emp id:'+id);
    }
    
}
class EmpThis{
    eid:any;
    constructor(id:any){
        this.eid=id;
        console.log('We are in Emp Constructor EmpThis id:'+this.eid);
    }
    
}
class EmpPara{
    fullName:any;
    constructor(public firstName:string,public lastName:string){
        this.fullName=firstName+' '+lastName
        console.log('We are in Emp Constructor EmpThis id:'+this.fullName);
    }
    
}
function greet(person:EmpPara){
   return 'Hello Mr.'+person.firstName+' '+person.lastName;
}


var object=new Employee();
var object1=new Emp(123);
var object2=new EmpThis(321);
var object3=new EmpPara('Rama','Krishna');
var result=greet(object3);
console.log(result);